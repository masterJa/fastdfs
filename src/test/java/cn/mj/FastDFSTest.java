package cn.mj;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.csource.common.MyException;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.StorageClient;
import org.csource.fastdfs.StorageServer;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerServer;
import org.junit.Before;
import org.junit.Test;

public class FastDFSTest {
	private static final String PATH = "http://192.168.0.200/fdfs/";
	
	/**
	 * 初始化
	 * @throws MyException 
	 * @throws IOException 
	 */
	@Before
	public void before() throws IOException, MyException {
		ClientGlobal.init("D:/java_software/workspace/spring-tool-suite/fastdfs/src/main/resources/fdfs-client.conf");
	}
	
	@Test
	public void upload() throws Exception {
		TrackerClient trackerClient = new TrackerClient();
		TrackerServer trackerServer = trackerClient.getConnection();
		StorageServer storageServer = null;
		StorageClient storageClient = new StorageClient(trackerServer, storageServer);
		File file = new File("C:/Users/Administrator/Desktop/哈哈.png");
		
		NameValuePair[] meta_list = null;
		String[] result = storageClient.upload_file(FileUtils.readFileToByteArray(file), FilenameUtils.getExtension(file.getName()), meta_list);
		System.out.println(Arrays.toString(result));
		System.err.println(PATH + result[0] + "/" + result[1]);
//		[group1, M00/00/00/wKgAy1oiwVaAKYVmAATEcaWyFkE806.png]
//		http://192.168.0.200/fdfs/group1/M00/00/00/wKgAy1oiwVaAKYVmAATEcaWyFkE806.png
	}
	
	
	@Test
	public void uploadAndAppend() throws Exception {
		TrackerClient trackerClient = new TrackerClient();
		StorageClient storageClient = new StorageClient(trackerClient.getConnection(), null);
		File file = new File("C:/Users/Administrator/Desktop/哈哈.png");
		String[] result = storageClient.upload_appender_file(FileUtils.readFileToByteArray(file), FilenameUtils.getExtension(file.getName()), null);
		System.out.println(Arrays.toString(result));
		System.err.println(PATH + result[0] + "/" + result[1]);
	}
	
	@Test
	public void append() throws Exception {
		TrackerClient trackerClient = new TrackerClient();
		StorageServer storeStorage = trackerClient.getStoreStorage(trackerClient.getConnection());
		System.out.println(storeStorage.getInetSocketAddress());
		StorageClient storageClient = new StorageClient(trackerClient.getConnection(), trackerClient.getStoreStorage(trackerClient.getConnection()));
//		StorageClient storageClient = new StorageClient(trackerClient.getConnection(), new StorageServer("192.168.0.203", 23000, 1));
//		先上传，故意把一个文件只上传一半
		File file = new File("C:/Users/Administrator/Desktop/哈哈.png");
		
		byte[] bs = FileUtils.readFileToByteArray(file);
//		byte[] bs1 = new byte[10];
//		for (int i = 0; i < 10; i++) {
//			bs1[i] = bs[i];
//		}
//		String[] result = storageClient.upload_file(bs1, FilenameUtils.getExtension(file.getName()), null);	
//		System.out.println(Arrays.toString(result));
//		[group1, M00/00/00/wKgAy1oix66AVNkkAAAACnow2R0154.png]
		//这样断点续传有点问题		java.net.SocketException: Connection reset by peer: socket write error
		int i = storageClient.append_file("group1", "M00/00/00/wKgAy1oix66AVNkkAAAACnow2R0154.png", bs);
		System.out.println(i);
	}
	
	@Test
	public void download() throws Exception {
		TrackerClient trackerClient = new TrackerClient();
		StorageClient storageClient = new StorageClient(trackerClient.getConnection(), null);
		byte[] bs = storageClient.download_file("group1", "M00/00/00/wKgAy1oiwVaAKYVmAATEcaWyFkE806.png");
		FileUtils.copyInputStreamToFile(new ByteArrayInputStream(bs), new File("C:/Users/Administrator/Desktop/下载的文件.png"));
	}
	
	
	@Test
	public void delete() throws Exception {
		TrackerClient trackerClient = new TrackerClient();
		StorageClient storageClient = new StorageClient(trackerClient.getConnection(), null);
		storageClient.delete_file("group1", "M00/00/00/wKgAy1oiwVaAKYVmAATEcaWyFkE806.png");
	}
	
	@Test
	public void test1() throws Exception {
		File file = new File("C:/Users/Administrator/Desktop/哈哈.png");
		
		byte[] bs = FileUtils.readFileToByteArray(file);
		byte[] bs1 = new byte[10];
//		bs1 = bs;
		System.out.println(bs.equals(bs1));
		for (int i = 0; i < 10; i++) {
			bs1[i] = bs[i];
		}
		
		System.err.println(Arrays.toString(bs));
		System.err.println(Arrays.toString(bs1));
	}
}